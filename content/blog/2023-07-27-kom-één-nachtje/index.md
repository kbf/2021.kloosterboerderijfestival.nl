---
date: 2023-07-27T08:57:14.854Z
title: Kom één nachtje
author: KBF Team
resources:
  - name: featured_image
    src: main_square.jpg
---
Op het verzoek van enkele mensen hebben we samen gekeken of er toch een mogelijkheid kon komen voor mensen die maar één nachtje aan willen sluiten. En we hebben goed nieuws: dat kan van vrijdag op zaterdag. Je bent dan vanaf een uur of 10 welkom om je tent op te zetten en sluit aan bij het middaggebed op vrijdag. 's Middags zijn er workshops en 's avonds is er een barbecue. Zaterdagochtend werken we op het terrein van de Abdijhof en na het middaggebed ruimen we alles op. Ben je nieuwsgierig en wil je de sfeer van het KloosterBoerderijFestival komen proeven? We zien je graag. De optie is er op het inschrijfformulier bijgezet.