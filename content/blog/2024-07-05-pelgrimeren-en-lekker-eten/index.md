---
date: 2024-07-05T19:53:53.732Z
title: Pelgrimeren en lekker eten
author: KBF Team
resources:
  - name: featured_image
    src: kbf-zwemvijver.jpeg
  - name: featured_image
    src: whatsapp-image-2023-08-13-at-21.22.10.jpeg
  - name: featured_image
    src: kbf-zwemvijver.jpeg
---
Pelgrimeren, is dat iets van vroeger of van nu? En wat heeft dat met vertrouwen en overgave te maken? We hebben in Taizé een inspirerende Duitse vrouw ontmoet die hier graag uit eigen ervaring over wil delen op het Kloosterboerderijfestival. Susanne is, samen met haar man Peter, in verschillende etappes van hun woonplaats nabij Stuttgart naar Assisi in midden-Italië gelopen. Wij zijn in ieder geval heel blij dat ze haar verhaal wil komen delen. En wees niet bang: Thijs kan uitstekend Duits, dus waar nodig zal hij voor vertaling zorgen.

Pelgrimeren is ook het doen met wat de omgeving je te bieden heeft. Afgelopen jaar bleek dat op het terrein van de Abdijhof vroeger een zwemvijver gelegen heeft. Een aantal deelnemers vond de vijver nog steeds prima geschikt om even af te koelen. Houd je ook wel van een verfrissende duik? Vergeet je zwemkleren dan niet.

Op pelgrimstocht eet je wat je krijgt aangeboden. Dat geldt voor ons KBF'ers ook: Ook dit jaar zal de warme maaltijd door de Abdijhof verzorgd worden. De hele week zullen we vegetarisch eten. Wil je graag veganistisch eten, dan is het fijn als je dat ons nog even meldt. Op zaterdag hebben we een vegetarische inspiratie-barbecue, waar je hopelijk veel nieuwe inspiratie op doet voor een heerlijke vega BBQ thuis. Heb je een top recept dat we niet mogen missen? Stuur ons een mailtje!

Heb je je nog niet ingeschreven? Je hebt nog twee weken om dit te doen.