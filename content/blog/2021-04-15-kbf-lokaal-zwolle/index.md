---
date: 2021-04-15T18:23:24.907Z
title: KBF Lokaal Zwolle
author: Nelleke
resources:
  - name: featured_image
    src: blog5.jpg
---
Wat hadden we graag gewild dat het KBF Lokaal in Zwolle het startschot was gebleken van een reeks mooie ontmoetingen door het hele land. Helaas mocht het niet zo zijn, desondanks vertel ik je graag hoe mooi het was!

Op een zonnige najaarsdag halverwege september waren we welkom in de tuin van het [Timon-huis in Zwolle](https://www.timon.nl/). De koffie stond klaar en met het binnendruppelen van de deelnemers groeide ook de hoeveelheid zelfgebakken lekkers. Voor sommigen was het zomerfestival nog niet zo lang geleden, voor anderen was deze dag de eerste kennismaking met het KBF. In het programma was bewust veel tijd ingepland voor ontmoeting, waardoor je de tijd had ook de plek, de bewoners en de workshopleiders te leren kennen, na te praten en ook te dromen over soortgelijke ontmoetingen op andere inspirerende plekken. Met 4 workshops in 2 rondes had deze dag niet alleen een gezellig festival-sfeer, maar was er ook aan inhoud geen gebrek. De maaltijd werd gemaakt met meegebrachte ingrediënten, in een stoofpot boven een vuurtje in de tuin. Aan het einde van de dag was er nog de mogelijkheid een Taizé-viering bij te wonen in de Grote Kerk van Zwolle. 

### Moderne Devotie - Mink de Vries

Mink nam een groepje mee op pad door Zwolle, langs diverse plekken die van belang zijn (geweest) voor de Moderne Devotie. Vandaag de dag is het [ontmoetingshuis Moderne Devotie](https://www.ontmoetingshuismodernedevotie.nl/) een pioniersplek waar het gedachtegoed van de Moderne Devotie vormgegeven wordt. Een plek om elkaar te voeden, inspireren en bemoedigen. Gemeenschapsvorming waarin plek is voor geloofsgesprek, geestelijke begeleiding en kennisoverdracht. 

### Godly Play & Thera

[Godly Play](https://www.godlyplay.nl/) is een werkvorm waarbij je op een creatieve en beeldende manier wordt uitgenodigd om een verhaal uit de bijbel te verbinden met je eigen verhaal. Zonder al te veel uitleg begon Thera te vertellen over Abraham die door God geroepen wordt, een onbekende toekomst tegemoet. In een soort zandbak met behulp van blokjes, stenen en poppetjes kwam het verhaal tot leven.  

### Moestuin Roos

Het boerderij-aspect van deze KBF-lokaal was te vinden in het bezoek aan de moestuin van Roos. Een uitleg over permacultuur, het uitwisselen van kennis en ideeën, en natuurlijk genieten van de vruchten die er te plukken waren. 

### Reisgenoten Johan ter Beek

Johan vertelde ons over ["Reisgenoten"](https://www.protestantsekerk.nl/thema/reisgenoten/). Reisgenoten is een manier van geloven, hopen en liefhebben die vorm krijgt door het doen van oefeningen. Deze oefeningen omvatten het hele leven, iedere oefening legt de nadruk op één van de vijf kernwaarden (links op de foto). Met deze oefeningen wordt niet zozeer de vraag "wat geloof ik?" beantwoord, maar de vraag "hoe geloof ik?". 

- - -

Op dit moment laten de corona-maatregelen het niet toe, maar toch willen we je vragen om eens na te denken over de mogelijkheden voor een KBF-lokaal bij jou in de buurt. Welke ingrediënten vormen KBF-lokaal? 

* **Inhoud**: het bevat tenminste 2 delen van de drieslag duurzaamheid - sociale gerechtigheid & spiritualiteit
* **Lokaal**: dat wil zeggen dat je deelnemers meeneemt in iets dat je zelf goed kent
* **Laagdrempelig**: als KBF willen we plekken creëren waar iedereen welkom is en mee kan doen
* **Low-budget**: de prijs is zo laag mogelijk, onder andere door zelf eten mee te nemen en hiervan te delen