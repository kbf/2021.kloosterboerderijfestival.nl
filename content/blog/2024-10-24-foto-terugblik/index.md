---
date: 2024-10-24T06:34:17.443Z
title: Foto terugblik KBF 2024
author: KBF Team
resources:
  - name: featured_image
    src: IMG_20240805_204544774_HDR.jpg
---
Nu de herfst dan echt begonnen is, is het heerlijk om terug te kijken naar het festival van afgelopen zomer! We hebben het goed gehad met mooi weer, een mooi programma en nog mooiere mensen als deelnemers! Hartelijk dank aan iedereen die heeft bijgedragen om het KBF vorm te geven! 

In de Taizé gebeden verwonderden we ons met Job over de schepping en luisterden naar de gelijkenissen van Jezus

Tijdens het werk kon jong en oud meedoen om de Abdijhof (nog) mooier te maken

Duurzaamheid: een workshop van Ineke van www.naaiatelierkrul.nl, waarna er de hele week nog ruimte was om te repareren en nieuwe creaties te maken van reststof, en in het weekend natuurlijk weer vegetarisch barbecuen!

Workshops van Zuster Catharina, van Susanne uit Duitsland die we in Taizé hadden leren kennen, en van Johan ter Beek (link) voor de nodige spirituele verdieping. 

En verder natuurlijk zalig nietsdoen, verhalen vertellen, schuilen voor de zon, dansen en spelen!

