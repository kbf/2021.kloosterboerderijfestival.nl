---
date: 2024-01-19T21:08:13.715Z
title: Sneeuw
author: KBF Team
resources:
  - name: featured_image
    src: whatsapp-image-2024-01-19-at-22.18.01.jpeg
---
Het is een prachtig gezicht als sneeuw als een deken het landschap toedekt. Het is als een onbeschreven wit vel papier, klaar voor een nieuw begin. Als je een keer begint te schrijven, of door de witte deken heen loopt is het toch anders. De editie van het KloosterBoerderijFestival in 2024 is niet meer als dat onbeschreven blad of die sneeuwdeken. Er zijn al ideeën en de locatie is vastgelegd. We zouden graag iets met kleding willen doen komende zomer. Heb jij een leuk idee voor een workshop voor dat of een ander thema? Volgende maand komen we als team samen, fijn als je ons even mailt zodat we jou hersenspinsel mee kunnen nemen in het grote plan. Na ons overleg zullen we laten weten waar we nog meer hulp bij nodig hebben.

In de witte wereld van vandaag kijken we graag vooruit naar een kleurrijke zomer. Voor alles is een tijd, ik ben blij met de wisseling van de seizoenen.