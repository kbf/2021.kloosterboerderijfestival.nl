---
date: 2023-11-25T10:37:09.336Z
title: dankbaarheid
author: KBF Team
resources:
  - name: featured_image
    src: kbf-bbq.jpeg
  - name: featured_image
    src: kbf-tractor.jpg
  - name: featured_image
    src: kbf-dankbaarheid.jpg
---
Het KloosterBoerderijFestival is al weer bijna vier maanden geleden. We kijken dankbaar terug op een mooie week, met inspirerende ontmoetingen en leuke workshops. En er is behoorlijk veel werk verzet op het terrein van de Abdijhof. Wat echt een hoogtepunt voor mij was, was het werken in de moestuin. De gup van twee moest er eerst niets van hebben, totdat hij ontdekte dat er schatten in de grond zaten: ‘eie, eie!’ riep hij vol enthousiasme en ik weet uit betrouwbare bron dat de aarde tot in zijn luier zat. Na deze ervaring was geen molshoop op het terrein meer veilig, want onze kleine KBF’er dacht dat in iedere hoop aarde aardappels groeiden. Waar ik ook echt van genoten heb, is de wandeling in de Loonse en Drunense duinen en het samen zingen en bidden in de kapel. Deze was heel geschikt voor onze groep en het was ook heel mooi dat er verschillende mensen die daar op het terrein wonen, bij ons voor gebed aansloten. Al met al hebben we op deze locatie zoveel pluspunten dat we besloten hebben hier nog een jaartje te blijven. We hopen dat jullie daar net zo enthousiast van worden als wij. Dan kan ik jullie bij deze ook vertellen dat de data voor het komende jaar vaststaan en wel van **5 tot 11 augustus 2024**. Weer een dagje langer zodat het meer de moeite waard is als je voor een weekend bij ons aan wilt sluiten.

Daarnaast hebben we nog ander nieuws: zuster Catharina, die vorig jaar een workshop heeft gegeven op het KBF, heeft vorige week zaterdag haar eeuwige gelofte gedaan. Zij heeft er dus voor gekozen om de rest van haar leven aan God te wijden. En in oktober hebben een aantal KBF’ers elkaar gezien op de conferentie Groengelovig. Sommigen van ons hadden een wel heel erg actief aandeel aan de workshop van Rozemarijn. En deze maand is er gezinsuitbereiding geweest bij Charlotte thuis. Gefeliciteerd! Genoeg reden dus voor dankbaarheid.