---
date: 2020-07-18T09:47:30.430Z
title: 'KBF presenteert editie 2020: BackToTheRoots'
author: KBF Team
resources:
  - name: featured_image
    src: img_6505.jpg
---
Het kloosterboerderijfestival gaat niet door, lang leve het kloosterboerderijfestival! Gedurende lang tijd leek het niet mogelijk te zijn om een evenement te organiseren deze zomer. Maar we hebben een manier gevonden om toch iets kbf-achtigs te laten plaats vinden, op veel kleinere schaal en conform alle huidige richtlijnen. Daarom gaan we niet zoals gepland voor de tweede maal naar [Abdij Koningshoeven](https://www.koningshoeven.nl/) in Tilburg, maar zijn we van 8 tot 14 augustus te gast op [de plek waar het allemaal begon](/about#history), op de [Eemlandhoeve](https://www.eemlandhoeve.nl/) in Bunschoten-Spakenburg. Daarom heet de KBF-2020 editie BackToTheRoots, omdat het allemaal net iets anders zal zijn, en omdat we terug gaan naar de wortels van het kbf. We hopen op een week vol mooie ontmoetingen en ruimte voor bezinning temidden van een oase in de polder.
