---
date: 2024-04-16T19:08:03.862Z
title: Taizé, samen onderweg
author: KBF Team
resources:
  - name: featured_image
    src: whatsapp-image-2024-04-09-at-18.17.10.jpeg
  - name: featured_image
    src: whatsapp-image-2024-04-16-at-21.33.10.jpeg
  - name: featured_image
    src: whatsapp-image-2024-04-11-at-18.33.20.jpeg
---
Taizé kreeg afgelopen week bezoek van ons: Nelleke, Thijs en Anneke. Samen onderweg is het thema van Taizé voor dit jaar. Een week lang hebben we onszelf ondergedompeld in de muziek, stilte, inleidingen, natuur en ontmoetingen van deze inspirerende plek. Voor kamperen was het ook in Frankrijk nog best een beetje koud, maar gelukkig waren er extra dekens te leen in El Abiodh. We hebben inspiratie opgedaan voor het nieuwe KBF en heel concreet bestaat dat in dit geval uit een aantal liederen die we graag met jullie zouden willen zingen, een Duits echtpaar, dat veel ervaring heeft met samen onderweg zijn, dat serieus overweegt te komen en een spel dat we met jullie willen spelen: Kellia, the risk of the desert. Het is een coöperatief bordspel uitgedacht door broeders van Taizé. Gezamenlijk bouw je een gemeenschap op, waar eten en drinken net zo belangrijk zijn als bidden. Het laat zien hoe we samen sterker staan en samen uitdagingen het hoofd kunnen bieden. Kortom samen op weg zijn. Nieuwsgierig? Komende zomer kun je het spel samen spelen met mensen die zich al door de spelregels geworsteld hebben en weten hoe je gezamenlijk overleeft. Tenminste, daarvoor moeten we hier thuis nog even aan de slag met de gevorderde variant.