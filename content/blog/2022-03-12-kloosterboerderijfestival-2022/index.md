---
date: 2022-03-12T13:35:17.402Z
title: KloosterBoerderijFestival 2022
author: Nelleke
resources:
  - name: featured_image
    src: gebed-kbf1.jpg
---
Krijg jij ook zo'n zin om met zonder jas naar buiten te gaan? De zon die schijnt, het groen dat weer tevoorschijn komt, het nodigt uit om weer actief te worden, inspiratie op te doen, plannen te maken en uit te voeren. Zo ook voor het KloosterBoerderijFestival. De afgelopen tijd hoorde ik van verschillende kanten dat de zin in een nieuw festival weer begint te borrelen! Als voorlopige datum kwamen we op 13-19 augustus, dus noteer die vast in je agenda! 

*Organisatie*

Naast veel goede zin is er ook heel veel concrete actie nodig om een fantastisch festival neer te zetten. En daarvoor zoeken we mensen! Een aantal bekende gezichten zullen opnieuw in het team plaatsnemen, maar er is ook plaats genoeg voor nieuwe gezichten! Wil jij deze editie mee organiseren? Reageer dan zo snel je kunt, dan nemen we contact met je op!

*Ruimte voor ontmoeting*

Het duurt nog even voor het augustus is, en misschien heb je wel zin om voor die tijd al anderen te ontmoeten. Het organiseren van een KBF-Lokaal is misschien wat veelgevraagd (mag wel!), maar het lijkt me mooi om in een volgende nieuwsbrief een activiteiten-agenda te geven van kleine mogelijkheden tot ontmoeting. Ik denk dan bijvoorbeeld aan het bezoek van iemands moestuin, een lijstje met verschillende locaties van Taizé-vieringen of het samen bezoeken van een actie of lezing. Heb je ook een idee? Laat weten wie/wat/waar :-) 

*Website*

De website is sinds afgelopen zomer niet meer aangepast, maar je vindt er nog wel veel foto's en verhalen van hoe mooi het was! Als het je leuk lijkt om de website up-to-date te maken/houden, mag je het ons ook laten weten.