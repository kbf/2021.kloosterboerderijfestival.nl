---
date: 2023-06-05T10:03:19.157Z
title: KBF 2023
author: Nelleke
resources:
  - name: featured_image
    src: abdijhof.jpg
---
Zet het alvast in je agenda! Het KloosterBoerderijFestival 2023 is van 7 tot 12 augustus op de [Abdijhof Marienkroon in Nieuwkuijk](https://www.abdijhof.com/) (in de buurt van Den Bosch). Zodra de tickets gekocht kunnen worden, zal er een nieuwsbrief gestuurd worden. Dus meld je snel aan voor onze nieuwsbrief!
