---
date: 2024-06-16T07:46:45.495Z
title: Leven in gemeenschap
author: KBF Team
resources:
  - name: featured_image
    src: kbf-ster-kapel.jpeg
  - name: featured_image
    src: kbf-zonnebloem.jpeg
  - name: featured_image
    src: kbf-team.jpeg
---
Het KloosterBoerderijFestival is als inspiratieplek ook een plaats om kennis te maken en te oefenen met leven in gemeenschap. Het gezamenlijk ritme van leven en bidden doet iets met mensen. Het heeft aantrekkingskracht, al eeuwen lang. Ook in onze tijd. Zuster Catharina was twee jaar geleden op het KBF om te delen over haar keuze om in gemeenschap met andere vrouwen te leven. Ze is dominicanes en heeft afgelopen jaar haar eeuwige gelofte gedaan om deze keuze te bevestigen. Zuster Catharina is vernoemd naar de heilige Catharina van Siena (1347-1380) die in haar tijd ook een gemeenschap om zich heen vormde. Zij was geen slotzuster, maar juist een heel maatschappelijk betrokken vrouw. Vanuit haar diepe band met Jezus zag ze om naar mensen, zonder onderscheid, en wel op zo’n manier dat anderen door haar geïnspireerd werden. Dat wil zuster Catharina ook doen. Ze komt naar het KBF om een workshop te geven waarbij we na het delen van haar verhaal op zoek zullen gaan naar onze eigen drijfveren. Want om succesvol een gemeenschap te kunnen vormen is het belangrijk je je gezamenlijke grond onderzoekt. Op ons verzoek komt ze terug voor de mensen die deze workshop ook graag mee willen maken. Wij kijken uit naar haar komst.