---
date: 2024-03-09T12:45:25.516Z
title: Geduld
author: KBF Team
resources:
  - name: featured_image
    src: whatsapp-image-2024-03-09-at-14.12.08.jpeg
---
Vandaag is het heerlijk weer om lekker in de tuin aan de slag te gaan. Her en der wat snoeien en oud blad van de vrouwenmantel knippen, een beetje opruimen. Kortom alles wat gedaan moet worden om het nieuwe seizoen te verwelkomen. Binnen is het eerste wonder al geschied en staan de eerste zaailingen al geduldig te wachten tot ze groot genoeg zijn om naar buiten te mogen. 

Geduldig zijn kost soms moeite. Thuis hebben we een laadpaal die onze auto op dit soort dagen van zonnestroom kan voorzien. Echter weigert hij dienst. Thijs heeft op aangeven van de fabrikant de kap van het apparaat gehaald en wat blijkt: er is een slak in gekropen en die is op de printplaat geëlektrocuteerd. Voor nu moeten we de mooie dagen geduldig aan ons voorbij laten gaan en wachten tot hij weer gemaakt wordt. Gelukkig hebben we ook andere manieren om van de zon te genieten.

Dat even wachten geldt ook voor het KBF, want we zijn drie weken geleden met ons team bij elkaar geweest en binnenkort kun je je in gaan schrijven voor de editie van deze zomer. Net zoals we in deze dagen in afwachting zijn van Pasen. Een mooie tijd van ons bewust naar God keren en afstand doen van het oude. We oefenen ons geduld, zijn waakzaam en wachten.