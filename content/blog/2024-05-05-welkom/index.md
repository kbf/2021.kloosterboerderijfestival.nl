---
date: 2024-05-05T18:34:17.443Z
title: Welkom
author: KBF Team
resources:
  - name: featured_image
    src: whatsapp-image-2024-05-05-at-20.12.29.jpeg
  - name: broodzakken
    src: whatsapp-image-2024-05-05-at-19.59.02.jpeg
---
Her en der verschijnen er stukjes over het KloosterBoerderijFestival in de media en worden mensen persoonlijk uitgenodigd. Aan alle nieuwe mensen die zo op de website komen, willen we zeggen: van harte welkom! Stukje bij beetje zullen we hier meer details over het festival bekend maken en andere leuke nieuwtjes delen.

Ineke van Naaiatelier Krul is enthousiast om samen met ons begin van de week het naaiatelier te openen en uitleg en voorbeelden te geven, zodat we op ieder moment van de week er verder mee aan de slag kunnen. Want we willen iedereen tijdens de week de kans geven zelf zijn kleding te repareren en naar huis te gaan met mooie broodzakken. Wij zelf gebruiken thuis onze set broodzakken al zo'n drie jaar. Volgens ons is het, naast beter voor de schepping, ook handiger in de vriezer, want je pakt nooit lucht mee in. Ineke schreef zelf een leuke aankondiging:

**\*Workshop Naaicafe\*** 

**Kleding repareren is niet alleen duurzaam en gezellig, maar ook een kleine actie tegen de wegwerpmaatschappij. Schuif aan bij het naaicafe 🧵**

**Helemaal klaar met plastic zakken?! Naai je eigen broodzak van de leukste 2e hands stoffen. Eindeloos her te gebruiken.**

Ben je nieuwsgierig geworden? Neem dan een kijkje op haar website:[ naaiatelierkrul.nl](http://naaiatelierkrul.nl)

Ik krijg er in ieder geval helemaal zin van. Net als het mooie weer van de afgelopen dagen. De meeste plantjes staan inmiddels in de volle grond en aan regen hebben ze ook geen gebrek. Zo kan het lekker groeien, net als de inhoud van het KBF. Nog niet alle workshops liggen vast, dus heb je nog een leuk idee: op het aanmeldformulier of via de mail kun je het ons laten weten. Daar kun je ook al je vragen kwijt die je na het lezen van de website nog hebt. 

Al die aandacht voor het KloosterBoerderijFestival is natuurlijk mooi en we willen graag iedereen welkom heten, maar wij willen daar als team een kleine disclaimer aan toevoegen. We zijn een relatief klein festival en kunnen niet onbeperkt groeien. Wil je graag mee, wacht dan niet te lang met inschrijven, want als het festival vol zit zullen we de inschrijvingen sluiten. Voor nu is er nog voldoende plaats, maar we delen dit om teleurstelling te voorkomen.

Zien we je dan? Welkom in ieder geval!
