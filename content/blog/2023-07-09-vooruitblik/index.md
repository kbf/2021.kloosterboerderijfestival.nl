---
date: 2023-07-09T14:22:28.851Z
title: Vooruitblik
author: Anneke
resources:
  - name: featured_image
    src: kbf-salade.jpeg
---
Nog een maand en dan zijn we te gast op de Abdijhof. Echt een week om naar uit te kijken. Het is fijn om onder andere bezielde idealisten te zijn en een ritme aangereikt te krijgen waarin je gewoon maar mee mag gaan. Ik kijk uit naar drie keer per dag zingen en het stil maken, drie keer per dag samen eten, waarbij er dit jaar hulp is van een echte kok. 's Morgens lekker aan het werk in de mooie omgeving van het oude klooster in Nieuwkuijk en 's middags inspiratie opdoen bij de workshops. Maar 's avonds na het avondgebed gaat mijn lampje wel zo'n beetje uit. Eigenlijk jammer, want het is heerlijk om bij het kampvuur te zitten kletsen of te luisteren naar een mooi voorleesverhaal.

Op het terrein waar we te gast zijn wonen ook mensen van Focolare, worden Oekraiense vluchtelingen opgevangen en zitten meerdere stichtingen. Zelf kende ik de locatie van de kinderkapel die ze daar hebben, aangrenzend aan hun kapel. De groene omgeving, de mooie laan met oude bomen en de brug die alleen voetgangers kan dragen zijn me van dat bezoek bijgebleven. Nu ik er in april weer was samen met Nelleke, hebben we een nog veel uitgebreidere rondleiding door de gebouwen en over het terrein gekregen. De plek om te kamperen ligt al vast, maar de ruimte om te bidden zal worden toegewezen aan de hand van het aantal deelnemers en de mobiliteit van die deelnemers. We kunnen de hele week zo inrichten dat deze toegankelijk is voor mensen in een rolstoel, maar als er zich niemand aanmeldt die in een rolstoel zit kunnen we in andere ruimtes terecht, met weer andere voordelen. We zijn dus tot een week van te voren heel flexibel en aanmelden is dan ook mogelijk tot een week van te voren. Met de organisatie komen wij in dat weekend dan ook alvast naar de Abdijhof om de laatste voorbereidingen te doen.

Ik kijk er naar uit om deze week samen met jullie te delen! Wil je je aanmelden maar twijfel je ergens over? Neem dan gerust contact op! Tot dan!