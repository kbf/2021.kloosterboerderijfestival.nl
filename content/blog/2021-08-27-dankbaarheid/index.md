---
date: 2021-08-27T11:37:46.376Z
title: Dankbaarheid!
author: "Inge"
resources:
  - name: featured_image
    src: foto-1-blog-inge-jantina-waterlelie.jpg
---
Ora et labora, bidden en werken dat gebeurt al eeuwen in de kloosters, maar ook op het KBF is het een vast onderdeel.  Eerst samen vieren met de prachtige liederen uit Taizé en 10 minuten stilte, en we bidden voor al het mooie wat God geschapen heeft. 

Daarna heerlijk de handen uit de mouwen en soms letterlijk met de handen in de aarde. Zo was één van de klussen brandnetels en onkruid weghalen. En zo kwamen we op een mooie plek terecht maar helaas hadden de bramenstruik en de brandnetels hier nogal de overhand gekregen. Met vereende krachten en de nodige oerkreten verzetten we veel werk. Na een uur onkruid weghalen was er ineens een picknicktafel zichtbaar en een betegelde vloer. Na nog 2 uur was het water grotendeels weer zichtbaar zelfs met een mooie waterlelie. Een enkele fanatiekeling ging zelfs in de middag nog verder om het karwei helemaal af te maken.

Wat is dit toch fijn en dankbaar werk, om te zien dat het in zo'n korte tijd zo opknapt! Niet alleen wij waren blij en dankbaar dat we dit konden doen ook de eigenaar van deze mooie plek, Jantina, een kwieke dame van in de 80, was erg blij dat wij dit voor haar hadden gedaan. 

Als dank ontvingen we de volgende dag een groot Spakenburgs hart van Jantina, met daarop de tekst: “Kloosterrijke handjes aan mijn tuinhuisje, dankjewel.”

En zo verspreidde de dankbaarheid zich over het hele KBF uit.
