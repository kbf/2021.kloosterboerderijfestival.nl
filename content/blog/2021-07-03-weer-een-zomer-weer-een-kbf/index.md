---
date: 2021-07-03T19:19:06.074Z
title: Weer een zomer, weer een KBF!
author: Nelleke
resources:
  - name: featured_image
    src: weidevolk.jpg
---
Ben je nog volledig onbekend met het KloosterBoerderijFestival? Lees dan [hier](**www.kloosterboerderijfestival.nl/about**) voor meer algemene informatie over onze doelen en dromen.

Hieronder vind je een uitleg over wat er dit jaar anders is dan afgelopen jaren. Het grootste verschil is dat we als organisatie ietsje minder draagkracht hebben, en daarom hopen dat het festival nog meer dan anders mede wordt gedragen door de deelnemers.

Terug samen bidden, werken, en elkaar inspireren met ons samenzijn. Dat is onze droom voor deze zomer. Na een periode van veel online contact zien we er enorm naar uit elkaar weer gewoon te kunnen ontmoeten. Een vast ritme van de Taizégebeden, ontspanning door inspanning tijdens het werk in de ochtend, en de middagen/avonden open om samen in te vullen. Veel vrije tijd en ruimte voor ontmoeting. [Hier vind je het dagprogramma](https://2021.kloosterboerderijfestival.nl/#schedule) (alleen de zondag wijkt af).

## Team

Als team zijn we weer enthousiast om deze editie te organiseren, al zijn we qua aantal en energie wat gekrompen ten opzichte van eerdere jaren. Toch willen we er voor gaan, dit jaar misschien iets meer bewust dan anders dat het festival vooral vorm krijgt met inzet van alle deelnemers. Zonder mensen geen festival! We werken niet met een corvee-rooster of takenlijsten, maar zullen wel met regelmaat een oproep doen voor hulp bij de afwas, wat schoonmaken of het verbouwen van een ruimte voor een workshop. Alleen samen krijgen we het voor elkaar!

## Inbreng deelnemers

Om het realistisch te houden qua organisatie en vooral ook om alle kracht en pracht die in onze groep zit te vieren, komen er waarschijnlijk geen/weinig externe sprekers. Jan en Maaike Huijgen van de Eemlandhoeve hebben veel kennis en enthousiasme met ons te delen, en daarnaast gaan we weer volop bijdragen en programma vanuit de groep laten komen. Praktische workshops, thema-tafels en gespreksgroepen, sardientje spelen, koeien vangen of rustig wandelen in de omgeving. Van alles hebben we voorgaande jaren voorbij zien komen, en we zijn benieuwd wat jij dit jaar voor inbreng hebt! Als je van tevoren al een idee hebt, kun je dit alvast aan ons laten weten. Ook handig als je bijvoorbeeld materiaal nodig hebt.

## Werk

Ora et Labora! Iedere morgen werken we van 9.30 tot 12.00. We doen erg ons best om veel diverse klussen aan te bieden, van fysiek zwaar (sloot uitrieten of muurtjes slopen) tot lichter (vijgen plukken of groente snijden). Zo hopen we dat iedereen op zijn/haar eigen manier een bijdrage kan leveren en iets terug kan geven aan de plek waar we welkom zijn. Omdat we weten dat dit niet voor iedereen haalbaar is, vragen we bij je aanmelding om een indicatie te geven of en hoeveel je verwacht te kunnen werken. Op die manier kunnen we ook de Eemlandhoeve een realistisch beeld geven van hoeveel werk we kunnen verzetten.

## Kamperen

Op de Eemlandhoeve hebben we alle ruimte om op diverse plekken tentjes neer te zetten, de één op een rustig plekje achteraf, de ander juist dichtbij alle bedrijvigheid. Voor de kinderen organiseren we geen programma, maar er is volop ruimte om te spelen en we kunnen gebruik maken van de aanwezige skelters. Op het terrein is ook een terras en een winkel, diverse mensen huren er bedrijfsruimte en er is een dagbesteding voor ouderen. Dit betekent veel gezelligheid, en belangstelling voor wat wij allemaal aan het doen zijn. ’s Avonds is het rustig en kun je tot in de late uurtjes bij het kampvuur zitten, of lekker vroeg je bedje in om de volgende dag (enigszins) uitgerust weer op te staan.

{{< rawhtml >}}

<a class="btn btn-lg btn-primary" href="https://registratie.kloosterboerderijfestival.nl/kbf/eemlandhoeve2021/">Meld je nu aan!</a>

{{< /rawhtml >}}

