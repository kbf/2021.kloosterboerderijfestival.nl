---
date: 2022-06-05T18:37:34.745Z
title: Voorbereiding KBF22
author: KBF Team
resources:
  - name: featured_image
    src: dsc02036_hudced8bcfe22292ced2a3e4887c9f0f23_134171_1200x0_resize_q75_box.jpg
---
Binnenkort komt er weer een nieuwsbrief aan! Heb jij nog nieuws wat daar zeker in moet komen? [Laat het ons weten](https://2021.kloosterboerderijfestival.nl/#contact)!

Wil jij helpen met het organiseren van het KBF komende zomer? Of je nu veel tijd hebt, of eigenlijk heel weinig, we horen het graag! Binnenkort hebben we een voorbereidingsweekend, misschien ben je daar wel graag (een gedeelte) bij voor brainstormen/dromen/praktische voorbereidingen? Ook dat horen we graag! Mag via zelfde e-mailadres.