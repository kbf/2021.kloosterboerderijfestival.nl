---
date: 2022-07-10T16:44:51.212Z
title: KloosterBoerderijFestivalVoorbereidingsweekend
author: Nelleke
resources:
  - name: Functie KBF
    src: blog-foto-.jpg
  - name: featured_image
    src: voorbereidingsweekend-groepsportret.jpg
  - name: featured_image
    src: kapel-wittenberg.jpg
  - name: featured_image
    src: vijver-wittenberg.jpg
  - name: featured_image
    src: wandelen-wittenberg.jpg
  - name: featured_image
    src: buiten-overleg-wittenberg.jpg
  - name: featured_image
    src: kruis-wittenberg.jpg
---
Ik ben net terug van het KBF-voorbereidingsweekend. Twee dagen op [De Wittenberg](https://www.dewittenberg.nl/) in Zeist om na te denken over hoe we willen dat het KloosterBoerderijFestival er komende zomer uit moet komen te zien. En direct ook aan de slag om die ideeën uit te voeren! En ik moet zeggen: dat is goed gelukt. 

We willen dat het festival een plek is waar je je kunt opladen, een plek waar het niet vreemd is om gelovig of groen te zijn, waar je bekenden terugziet. Tegelijkertijd willen we een plek zijn waar onbekenden welkom zijn, dat het programma er aan bijdraagt dat je makkelijk aan kunt haken en mee kunt doen. Zodat iedereen gezien wordt, en onbekenden in rap tempo in bekenden veranderen. 

Daarom hebben we iedere middag een kort extra programma onderdeel toegevoegd, waar iedereen kan aansluiten. Wat dat is, kun je binnenkort op de website zien bij het dagprogramma :) 

Daarnaast wordt bij het voorbereiden ook steeds weer duidelijk dat we jullie als deelnemers nodig hebben om het KBF te laten slagen. Diverse taken moeten nog verdeeld worden, voor sommige zullen we van tevoren mensen vragen, maar voor andere kun je je ter plekke inschrijven. Naast oplaadplek zien we KBF namelijk ook als een oefenplek, waar je leert door te doen en waar fouten maken mag. 

Op de website verschijnen binnenkort ook de leuke hoofden van de teamleden van dit jaar, en wat foto's van dit voorbereidingsweekend. Omdat we [De Wittenberg](https://www.dewittenberg.nl/) beloofd hebben om door te vertellen wat een fijne plek het is om te verblijven. En om je jaloers te maken. Dat ook.

Liefs en graag tot ziens deze zomer!

Nelleke
