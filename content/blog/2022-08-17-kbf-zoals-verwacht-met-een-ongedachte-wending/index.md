---
date: 2022-08-17T10:51:04.461Z
title: "KBF zoals verwacht, met een ongedachte wending.. "
author: KBF Team - Nelleke
resources:
  - name: featured_image
    src: kbf-mededelingen-blog.jpg
---
Dag leuke lezer!

Als team hebben we vooraf enorm uitgezien naar een gewoon KBF, na 2 jaar vol corona-beperkingen. En onze verwachtingen komen uit! Fijne ontmoetingen, inspirerende sprekers, muziek, heerlijk eten, de voeten in de modder. KBF zoals we het graag zien.

Naast 25 oude bekenden, zijn er dit jaar ook 11 nieuwe gezichten geteld! (naast de baby-gezichtjes die vorig jaar ook nog onbekend waren :-))

Live-muziek, hier op de foto Trio Kefi. Zondagavond genoten we van 'TempelTechno' van Johan ter Beek, en de avond daarvoor is er goed gedanst op de bruiloftsmuziek van de buren, maar dat was stiekem dus daar zijn geen foto's van :-D

De onverwachtse wending zie je op de welbekende zelftest. Vol ongeloof heb ik hier naar zitten kijken, maar ik schrijf deze blog toch echt vanuit huis, terwijl het festival nog in volle gang is. Ik hoop van harte dat het blijft bij de 2 besmettingen die we geconstateerd hebben. 

Graag zien we jullie volgend jaar, of liever nog eerder bij een KBF-Lokaal of KBF-weekend, maar daarover later meer :)

Dikke digitale knuffel!
