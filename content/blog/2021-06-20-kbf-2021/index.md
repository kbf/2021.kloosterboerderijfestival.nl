---
date: 2021-06-20T10:03:19.157Z
title: KBF 2021
author: Nelleke
resources:
  - name: featured_image
    src: img_6505.jpg
---
Zin in KBF? Check. Datum? Check. Locatie? Check. Deelnemers? Check. En verder? Uhm..?!

Krijg jij met deze zomerse temperaturen ook zo’n zin in een zonnig kloosterboerderijfestival? De datum hadden we al geprikt (13-20 augustus!), maar nu is er nog meer goed nieuws te melden! We zijn welkom op de Eemlandhoeve! We zijn heel blij dat we opnieuw op deze bijzondere plek ons festival mogen organiseren. 

Van verschillende kanten horen we dat er belangstelling is er deze keer (weer) bij te zijn. We zien er enorm naar uit om oude contacten die we afgelopen jaar (veel) te weinig gezien hebben weer te ontmoeten, en nieuwe mensen te leren kennen.

Wat is er nog meer nodig naast een datum, locatie en deelnemers? Organisatie! En daarom zijn we op zoek naar JOU! Vind jij het leuk om vooraf de Eemlandhoeve al te bezoeken om verdere afspraken te maken? Wil jij wel een heerlijk weekmenu samenstellen? Ben jij goed in het bijhouden van de aanmeldingen en bijbehorende administratie? Speel jij graag een dag voor kok? Heb jij een idee voor een inspirerende spreker? Neem jij materiaal mee om anderen enthousiast te maken over je hobby tijdens een workshop? Lijkt het je wel wat om tijdens de week een keertje tijdsbewaker of etensverantwoordelijke te zijn? Neem jij initiatief voor een avondje dans en muziek? Coördineer jij graag het werk? Ben jij zangleider tijdens het gebed? Of… wat dan ook?

[Laat het ons weten](https://2021.kloosterboerderijfestival.nl/#contact)! Al heb je maar een klein idee of één tip, al ben je al een natuurtalent op een bepaald gebied en wil je dat graag inzetten, al heb je ergens 0 verstand van, maar wil je graag een uitdaging waarin je iets nieuws leert.. we horen het allemaal ontzettend graag. Ook als je wel iets wilt doen, maar niet alleen of niet te veel, zet dat er gewoon bij 😊 Ik wil zelf bijvoorbeeld graag langs de Eemlandhoeve, maar vind dat in mijn eentje wat ongezellig..!

Ik ben nieuwsgierig hoe we deze editie samen vorm gaan geven, en ik hou de mail in de gaten! Kom maar door!

Warme groet namens het team,

Nelleke
