---
date: 2021-08-17T11:56:44.772Z
title: Foto-impressie weekend
author: Mattias
resources:
  - name: featured_image
    src: DSCF4371.jpg
---
Foto-impressie van de eerste paar dagen gemaakt door Marieke, Sofie, Mattias, Roos en Nelleke.
