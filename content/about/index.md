---
title: Over het festival
type: about
---

## Doelstelling
Het KloosterBoerderijFestival wil jongeren samenbrengen, als netwerk, en specifiek in een week in de zomer. Er is een ritme van gebed (3 keer per dag), werken (elke ochtend) en sprekers. Bidden-werken-denken-vieren. De doelgroep zijn jongeren die begaan zijn met de wereld, op het gebied van duurzaamheid, sociale gerechtigheid en barmhartigheid, omdat ze geraakt zijn door Jezus’ voorbeeld hierin.

Het doel is om mensen te bezielen, om het werk van barmhartigheid dat ze doen voort te zetten, of om bezielt te raken om iets nieuws te beginnen.

## Doelgroep
De doelgroep zijn jongeren die begaan zijn met de wereld, op het gebied van duurzaamheid, sociale gerechtigheid en barmhartigheid, geïnspireerd door Jezus’ voorbeeld hierin. In de praktijk is de leeftijd van de deelnemers rond de 25-35, maar in principe is iedereen welkom. De deelnemers komen uit verschillende kerkelijke stromingen of hebben ze een niet-kerkelijke achtergrond en wonen in verschillende delen van het land. We merken dat we ook steeds meer jonge gezinnen aantrekken, en doen onze best hen een plek te geven en ontmoeting tussen beide groepen te realiseren.

## Activiteiten
Het KloosterBoerderijFestival is een festival van zeven dagen aan het eind van de zomer. Elke dag is er ochtend-, middag- en avondgebed. 's Ochtends wordt er gewerkt in en om het klooster, waaronder boerderijwerk, 's middags zijn er workshops, groepsgesprekken en lezingen over thema’s als duurzaamheid, vredeswerk, gemeenschapsleven en spiritualiteit. 's Avonds is er tijd voor muziek, films en kampvuurtjes.

## Uitvoering
Ten eerste is het KloosterBoerderijFestival een plek om alvast te ervaren hoe een christelijk geïnspireerd leven eruit kan zien. We bidden samen, werken op het land, en ontmoeten elkaar, we oefenen in een levensstijl die rekening houdt met de schepping en de armen, onder andere door fairtrade en biologisch te eten. Ten tweede is het KloosterBoerderijFestival een plaats waar (jonge) christenen in contact gebracht worden met verschillende sprekers en organisaties die zich vanuit hun geloof inzetten voor de wereld. Zo maken deze jongeren kennis met nieuwe opties, die soms heel concreet worden. Zo zijn er meerdere mensen geweest die na het festival ook nog eens een tijd stage zijn gaan lopen in een van de projecten die ze daar ontmoet hebben, zoals de [Eemlandhoeve](https://www.eemlandhoeve.nl/), [Nieuw Sion](https://www.nieuwsion.nl/), [Taizé](https://www.taize.fr/), [Casella](https://www.casella.nl/), [Overhoop](http://leveninovervecht.nl/overhoop/) en de [Catholic Worker in Calais](https://mariaskobtsova.org/). Ook is onderlinge inspiratie belangrijk. Veel van de deelnemers zetten zich op de een of andere manier in voor een betere wereld, er zijn mensen die ongedocumenteerden opvangen in huis, mensen die in een christelijke woongroep wonen, mensen die vrijwilligerswerk met daklozen doen of met vluchtelingen. In de ontmoeting leidt dit tot vruchtbare kruisbestuivingen en het ‘ik ben niet de enige’ gevoel. Zo zetten we jongeren actief aan het denken over hoe ze hun geloof actief kunnen maken.
